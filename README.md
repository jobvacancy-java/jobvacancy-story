## Ajuste del look & feel

Apenas generada la aplicación, tiene el look & feel default de JHipster, con lo cual comenzamos ajustando esto.

1. Ajustamos la portada de la aplicación modificando el archivo src/main/webapp/scripts/app/main/main.html
2. Agregamos una nueva hoja de estilo src/main/webapp/assets/styles/jobvacancy.css
3. Agregamos la nueva hoja de estilos al arhcivo src/main/webapp/index.html
4. Y ya que estamos ajustamos el footer también en el archivo src/main/webapp/index.html

Todos estos cambios podemos verlos en el commit 85de787.



Backlog
=======

Como oferente quiero publicar mis ofertas de trabajo. => Publicación de oferta


Publicación de oferta
=====================

1. yo jhipster:entity offer
 title String required
 location String required
 description String max len
 user User many-to-one


Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   src/main/resources/config/liquibase/master.xml
        modified:   src/main/webapp/index.html
        modified:   src/main/webapp/scripts/components/navbar/navbar.html

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .jhipster/
        src/main/java/com/jobvacancy/domain/Offer.java
        src/main/java/com/jobvacancy/repository/OfferRepository.java
        src/main/java/com/jobvacancy/web/rest/OfferResource.java
        src/main/resources/config/liquibase/changelog/20151203191715_added_entity_Offer.xml
        src/main/webapp/scripts/app/entities/offer/
        src/main/webapp/scripts/components/entities/
        src/test/gatling/simulations/
        src/test/java/com/jobvacancy/web/rest/OfferResourceTest.java
        src/test/javascript/spec/app/entities/

no changes added to commit (use "git add" and/or "git commit -a")


Correr los tests: mvn test y grunt test para verificar que todo continua bien.
Ejecutamos la aplicaciones y vemos que la funcionalidad de creación de ofertas funciona está ahora disponible aunque require algunos cambios:

1. El campo ID no deberia mostrarse en la vista de creación. Esto requiere modificar el archivo src/main/webapp/scripts/app/entities/offer/offer-dialog.html

2. Al crear una oferta, la misma pertenece al usuario que la está creando y el debe poder verla y manipularla. Esto requiere modificar:
src/main/webapp/scripts/app/entities/offer/offer-dialog.html => para no mostrar el campo usuario
src/main/java/com/jobvacancy/web/rest/JobOfferResource.java => para que el usuario actual sea asociado a la oferta en creación
src/test/java/com/jobvacancy/web/rest/OfferResourceTest.java => para actualizar los testsgit 

Para probar esto vamos a desactivar momentaneamente la activación de usuarios. 94c0163
Ejecutamos la aplicación, entramos con el usuario user, creamos una oferta y salimos del sistema. Al hacer notamos que la página de inicio tiene un pequeño issue de estilo, tomamos nota para arreglarlo más tarde.
Nos registramos con un usuario user2, entramos a la aplicación y vemos la oferta creada bajo el otro usuario, lo cual no está bien, pues a esta altura cada oferta solo deberia estar visible para su creador. Antes de pasar a arreglar esto, aplicamos el fix de estilo ()


Public Offers
=============

Vamos a comenzar agregar un nuevo controller (Resource) para permitir el acceso a las ofertas públicas.

1. Creamos PublicOfferResource y su correspondiente test e1f78b8
2. Ajustamos la barra de navegación: src/main/webapp/scripts/components/navbar/navbar.html
3. Creamos los componentes angular: definición de estados, controller, vistas y servicio
        new file:   src/main/webapp/scripts/app/entities/publicoffer/publicoffer.controller.js
        new file:   src/main/webapp/scripts/app/entities/publicoffer/publicoffer.html
        new file:   src/main/webapp/scripts/app/entities/publicoffer/publicoffer.js
        new file:   src/main/webapp/scripts/components/entities/publicoffer/publicoffer.service.js
4. Incluimos los nuevos componentes en la aplicación modified: src/main/webapp/index.html

