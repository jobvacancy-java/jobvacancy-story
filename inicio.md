
En términos muy generales nuestro cliente queria que construyéramos un tablero de empleos. Luego de una charla inicial y aprovechando la estadía de nuestro cliente en 

John Smith, estadounidense, emprededor, amante de la tecnología y los viajes y cliente nuestro desde hace 1 año. Nos conocimos en un Open Space en Buenos Aires en 2014, John estaba haciendo turismo por sudamérica y quería conocer empresas de desarrollo y freelancers que pudieran ayudarlo en la implementación de sus empredimientos. 


......

Para tener una idea general del alcance del proyecto en términos funcionales hicimos una sesión de Story Mapping.
npm
Nuestro cliente era de USA y por ello decidimos utilizar idioma inglés en todos los artefactos del proyecto, desde las user stories hasta los artefactos de código.


John no tiene preferencias técnicas estrictas aunque prefiere que trabajemos con tecnologias open source. Por otro lado ha sido muy claro respecto de sus expectativas a nivel de interface de usuario: quiere algo responsive y que funcione bien en las últimas 3 versiones de los principales navegadores (Internet Explorer, Chrome, FireFox y Safari).


Respecto de la tecnología todos estamos de acuerdo en utilizar AngularJS para la presentación web, pero respecto de la parte server-side no estamos del todos de acuerdo. Básicamente tenemos dos posiciones: Ruby o Java. Personalmente me siento más cómodo con Ruby pues el lenguaje me gusta más pero al mismo tiempo herramienta Java que vengo siguiendo desde hace un tiempo y con la he hecho algunas pruebas de conceptos y desarrollos caseros: JHipster.Esta herramienta creada por Julien Dubois es básicamente un generador de código (un Yeoman) que genera aplicaciones usando AngularJS para la presentación y Spring-Boot para la parte server-side. Al mismo tiempo, integra un conjunto de herramientas que facilitan el uso de ciertas prácticas ágiles como TDD, BDD y Entrega continua. Le cuento al equipo 


Stack de tecnologias y herramientas
===================================

* Herramienta de Build server-side: Maven
* Herramienta de Build client-side: Grunt
* Herramienta de control de versiones: Git
* Servidor de integración continua: Jenkins
* Navegador: FireFox + addins: firebug y developer tools

* Servicio de control de versiones: GitLab
* Servicio de integración continua: Servidor propio corriendo en Digital Ocean




Creación del proyecto
=====================

La creación del proyecto (a nivel de código fuente) consiste básicamente en ejecutar el generador de código de JHipster. El mismo nos realizará una seria de preguntas (alrededor de 15 pero podrían ser más o menos dependiendo de la versión de JHipster que estemos utilizando). Una vez contestadas las preguntas JHipster generará un conjunto de archivos y ejecutar npm y bower para resolver las dependencias JavaScript de nuestra aplicación.

Una vez creada la estructura ejecuto maven y grunt para builder el proyecto a nivel server y client side respectivamente:

```
mvn
``


